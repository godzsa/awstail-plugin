package tech.central.ai.components;

import com.amazonaws.auth.profile.ProfilesConfigFile;
import com.intellij.openapi.fileChooser.FileChooserDescriptor;
import com.intellij.openapi.fileChooser.ex.FileTextFieldImpl;
import com.intellij.openapi.fileChooser.ex.LocalFsFinder;
import com.intellij.openapi.fileChooser.impl.FileChooserFactoryImpl;
import tech.central.ai.utils.PathUtil;

import javax.swing.*;
import java.awt.*;
import java.io.File;

public class ProfileSettingsForm extends JPanel {
    private JComboBox profileSelect;
    private JPanel content;
    private JLabel error;
    private JTextField awsConfigPath;

    public ProfileSettingsForm() {
        awsConfigPath.addCaretListener(a -> {
            refreshLocation();
        });

        setLayout(new BorderLayout());
        add(content);
        refreshLocation();
    }

    private void refreshLocation() {
        error.setText("");
        profileSelect.removeAllItems();

        try {
            new ProfilesConfigFile(new File(PathUtil.Companion.getPath(awsConfigPath.getText())).getAbsoluteFile())
                    .getAllBasicProfiles().keySet()
                    .forEach(profileSelect::addItem);
        } catch (Exception e) {
            if (e.getMessage().contains("AWS credential profiles file not found in the given path")) {
                error.setText("No credentials file found at the given path");
            } else if (e.getMessage().contains("Invalid property format: no '=' character is found in the line [client]")) {
                error.setText("File is not a valid AWS configuration file"); //TODO add HELP
            }
        }
    }

    public JPanel getContent() {
        return content;
    }

    public JTextField getAwsConfigPath() {
        return awsConfigPath;
    }

    public JComboBox getProfileSelect() {
        return profileSelect;
    }

    private void createUIComponents() {
        //TODO: refactor
        FileTextFieldImpl.Vfs myPathTextField = new FileTextFieldImpl.Vfs(
                new JTextField("~/.aws/credentials"),
                FileChooserFactoryImpl.getMacroMap(), null,
                new LocalFsFinder.FileChooserFilter(new FileChooserDescriptor(true, false, false, false, false, false), true));
        awsConfigPath = myPathTextField.getField();
    }
}
