package tech.central.ai.components;

import javax.swing.*;
import java.awt.*;

public class CustomSettingsForm extends JPanel {
    private JTextField accessKeyInput;
    private JTextField secretKeyInput;
    private JTextField roleArnInput;
    private JPanel content;

    public CustomSettingsForm() {
        setLayout(new BorderLayout());
        add(content);
    }

    public JPanel getContent() {
        return content;
    }

    public JTextField getAccessKeyInput() {
        return accessKeyInput;
    }

    public JTextField getSecretKeyInput() {
        return secretKeyInput;
    }

    public JTextField getRoleArnInput() {
        return roleArnInput;
    }
}
