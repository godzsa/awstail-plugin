package tech.central.ai.components;

import com.github.lgooddatepicker.components.DatePickerSettings;
import com.github.lgooddatepicker.components.DateTimePicker;
import com.github.lgooddatepicker.components.TimePickerSettings;
import com.intellij.openapi.fileChooser.FileChooserDescriptor;
import com.intellij.openapi.fileChooser.ex.FileTextFieldImpl;
import com.intellij.openapi.fileChooser.ex.LocalFsFinder;
import com.intellij.openapi.fileChooser.impl.FileChooserFactoryImpl;
import com.intellij.openapi.project.Project;
import org.fest.util.Strings;
import tech.central.ai.awstail.AWSTailArguments;

import javax.swing.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class SelectLogGroupComponent {
    private List<String> logGroupNames;

    public JTextField filterInput;
    public JButton okButton;
    private JCheckBox followCheckBox;
    private JComboBox logGroupNameSelect;
    private JLabel logGroupNameLabel;
    public JPanel layout;
    private DateTimePicker untilPicker;
    private DateTimePicker sincePicker;

    public SelectLogGroupComponent(List<String> logGroupNames, Project project) {
        this.logGroupNames = logGroupNames;

        filterList();

        filterInput.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent keyEvent) {
                super.keyTyped(keyEvent);
                filterList();
            }
        });
    }

    private void filterList() {
        List<String> keywords = Arrays.asList(filterInput.getText().split(","));
        logGroupNameSelect.removeAllItems();

        logGroupNames.stream()
                .filter(name -> keywords.stream().map(String::trim).allMatch(name::contains))
                .sorted(Comparator.comparingInt(String::length))
                .forEach(logGroupNameSelect::addItem);
    }

    public AWSTailArguments getAWSTailArgs() {
        return new AWSTailArguments(
                followCheckBox.isSelected(),
                ((String) logGroupNameSelect.getSelectedItem()),
                null,
                null,
                getDateTimeAtUTC(sincePicker.getDateTimePermissive()),
                getDateTimeAtUTC(untilPicker.getDateTimePermissive()),
                100,
                false,
                false,
                null
        );
    }

    public String getTabTitle() {
        String text = filterInput.getText();
        if (Strings.isNullOrEmpty(text)) {
            text = ((String) logGroupNameSelect.getSelectedItem());
        }

        if (followCheckBox.isSelected()) {
            return text + " - follow";
        } else {
            return text;
        }
    }

    private Long getDateTimeAtUTC(LocalDateTime dateTime) {
        return dateTime != null ? dateTime.atZone(ZoneId.systemDefault()).withZoneSameInstant(ZoneOffset.UTC).toEpochSecond() : null;
    }

    private void createUIComponents() {
        sincePicker = new DateTimePicker();
        untilPicker = new DateTimePicker();
    }
}
