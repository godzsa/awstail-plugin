package tech.central.ai.components;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.RegionUtils;

import javax.swing.*;
import java.awt.event.ItemEvent;
import java.util.HashMap;
import java.util.Map;

public class SettingComponent {
    public static final String PROFILE = "Profile (Credentials File)";
    public static final String CUSTOM = "Custom";
    public static final String ENV = "Environment Variables";

    private JPanel layout;
    private JComboBox<String> credentialTypeSelect;
    private JPanel content;
    private JComboBox regionSelect;

    private Map<String, JPanel> contents = new HashMap<>();

    public SettingComponent() {
        credentialTypeSelect.addItem(PROFILE);
        credentialTypeSelect.addItem(CUSTOM);
        credentialTypeSelect.addItem(ENV);
        credentialTypeSelect.addItemListener(this::switchLayout);

        RegionUtils.getRegions().stream()
                .map(Region::getName)
                .forEach(regionSelect::addItem);

        contents.put(PROFILE, new ProfileSettingsForm());
        contents.put(CUSTOM, new CustomSettingsForm());
        setLayoutBasedOnSelection(((String) credentialTypeSelect.getSelectedItem()));
    }

    public JPanel getLayout() {
        return layout;
    }

    private void switchLayout(ItemEvent event) {
        content.removeAll();
        setLayoutBasedOnSelection((String) event.getItem());
    }

    private void setLayoutBasedOnSelection(String layoutName) {
        if (contents.containsKey(layoutName)) {
            content.add(contents.get(layoutName));
        }
    }

    public JPanel getContent() {
        return content;
    }

    public JComboBox<String> getCredentialTypeSelect() {
        return credentialTypeSelect;
    }

    public Map<String, JPanel> getContents() {
        return contents;
    }

    public JComboBox getRegionSelect() {
        return regionSelect;
    }
}
