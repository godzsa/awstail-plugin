package tech.central.ai.components

import com.intellij.execution.filters.TextConsoleBuilderFactory
import com.intellij.execution.ui.ConsoleView
import com.intellij.execution.ui.ConsoleViewContentType
import com.intellij.openapi.project.Project
import com.intellij.uiDesigner.core.GridConstraints
import com.intellij.uiDesigner.core.GridLayoutManager
import javax.swing.JLabel
import javax.swing.JPanel


class LogsComponent(val project: Project) : JPanel(GridLayoutManager(2, 1)) {
    val title: JLabel = JLabel()
    private lateinit var console: ConsoleView

    init {
        initTitle()
        initTextPane()

        addText("Loading...")
    }

    private fun initTextPane() {
        console = TextConsoleBuilderFactory.getInstance().createBuilder(project).console
        console.allowHeavyFilters()

        val textPaneConstraints = GridConstraints()
        textPaneConstraints.row = 1
        textPaneConstraints.fill = GridConstraints.FILL_BOTH
        add(console.component, textPaneConstraints)
    }

    private fun initTitle() {
        val labelConstraints = GridConstraints()

        labelConstraints.row = 0
        labelConstraints.vSizePolicy = GridConstraints.SIZEPOLICY_FIXED
        labelConstraints.anchor = GridConstraints.ANCHOR_WEST
        add(title, labelConstraints)
    }

    fun addText(line: String) {
        console.print(line, ConsoleViewContentType.NORMAL_OUTPUT)
    }

    fun setHeader(text: String) {
        title.text = """ $text"""
    }
}