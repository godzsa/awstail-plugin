package tech.central.ai.settings

import com.intellij.openapi.components.PersistentStateComponent
import com.intellij.openapi.components.ServiceManager
import com.intellij.openapi.components.State
import com.intellij.openapi.project.Project

@State(name = "AWSTailSettings")
class AWSTailPersistentStateComponent : PersistentStateComponent<AWSTailPersistentStateComponent.AWSTailSettingsState> {
    companion object {
        fun getInstance(project: Project): AWSTailPersistentStateComponent =
                ServiceManager.getService(project, AWSTailPersistentStateComponent::class.java)
                        ?: AWSTailPersistentStateComponent()
    }

    private var awsTailSettingsState = AWSTailSettingsState()

    override fun getState(): AWSTailSettingsState {
        return awsTailSettingsState
    }

    override fun loadState(state: AWSTailSettingsState) {
        awsTailSettingsState = state;
    }

    data class AWSTailSettingsState(
            var credentialType: String = "",
            var awsConfigPath: String = "~/.aws/credentials",
            var profile: String = "",
            var roleArn: String = "",
            var accessKey: String = "",
            var secretKey: String = "",
            var region: String = ""
    )
}