package tech.central.ai.settings

import com.intellij.openapi.options.SearchableConfigurable
import com.intellij.openapi.project.Project
import tech.central.ai.AWSTailPlugin
import tech.central.ai.components.CustomSettingsForm
import tech.central.ai.components.ProfileSettingsForm
import tech.central.ai.components.SettingComponent
import javax.swing.JComponent

class AWSTailConfigurable(val project: Project) : SearchableConfigurable {
    private val state = AWSTailPersistentStateComponent.getInstance(project).state
    private val settingComponent = SettingComponent()

    override fun getDisplayName(): String = "AWS Tail Configurations"
    override fun getId(): String = "AWS Tail config"

    override fun createComponent(): JComponent {
        loadSettings()
        return settingComponent.layout
    }

    override fun apply() {
        saveSettings();
        AWSTailPlugin.restart(project);
    }

    override fun reset() {
        loadSettings();
    }

    override fun isModified(): Boolean {
        val savedState = AWSTailPersistentStateComponent.getInstance(project).state
        val profileSettingsForm = settingComponent.contents[SettingComponent.PROFILE] as? ProfileSettingsForm
        val customSettingsForm = settingComponent.contents[SettingComponent.CUSTOM] as? CustomSettingsForm

        return savedState.credentialType.equals(settingComponent.credentialTypeSelect.selectedItem as String?)
                .and(savedState.awsConfigPath.equals(profileSettingsForm?.awsConfigPath?.text))
                .and(savedState.profile.equals(profileSettingsForm?.profileSelect?.selectedItem as String?))
                .and(savedState.accessKey.equals(customSettingsForm?.accessKeyInput?.text))
                .and(savedState.secretKey.equals(customSettingsForm?.secretKeyInput?.text))
                .and(savedState.roleArn.equals(customSettingsForm?.roleArnInput?.text))
                .and(savedState.region.equals(settingComponent.regionSelect.selectedItem as String?))
                .not()
    }

    private fun saveSettings() {
        val profileSettingsForm = settingComponent.contents[SettingComponent.PROFILE] as? ProfileSettingsForm
        val customSettingsForm = settingComponent.contents[SettingComponent.CUSTOM] as? CustomSettingsForm

        state.awsConfigPath = profileSettingsForm?.awsConfigPath?.text ?: ""
        state.profile = profileSettingsForm?.profileSelect?.selectedItem as String? ?: ""
        state.accessKey = customSettingsForm?.accessKeyInput?.text ?: ""
        state.secretKey = customSettingsForm?.secretKeyInput?.text ?: ""
        state.roleArn = customSettingsForm?.roleArnInput?.text ?: ""
        state.region = settingComponent.regionSelect.selectedItem as String
        state.credentialType = settingComponent.credentialTypeSelect.selectedItem as String
    }

    private fun loadSettings() {
        val profileSettingsForm = settingComponent.contents[SettingComponent.PROFILE] as? ProfileSettingsForm
        val customSettingsForm = settingComponent.contents[SettingComponent.CUSTOM] as? CustomSettingsForm

        profileSettingsForm?.awsConfigPath?.text = state.awsConfigPath
        profileSettingsForm?.profileSelect?.selectedItem = state.profile
        customSettingsForm?.accessKeyInput?.text = state.accessKey
        customSettingsForm?.secretKeyInput?.text = state.secretKey
        customSettingsForm?.roleArnInput?.text = state.roleArn
        settingComponent.regionSelect.selectedItem = state.region
        settingComponent.credentialTypeSelect.selectedItem = state.credentialType
    }
}