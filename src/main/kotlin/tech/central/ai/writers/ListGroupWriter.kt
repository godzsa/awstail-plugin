package tech.central.ai.writers

import com.amazonaws.services.logs.model.LogGroup
import tech.central.ai.awstail.AWSLogsClient
import tech.central.ai.awstail.writers.GroupWriter

class ListGroupWriter(awsLogs: AWSLogsClient, var logGroupNames: MutableList<String>) : GroupWriter(awsLogs) {
    override fun writeGroupName(logGroup: LogGroup) {
        logGroupNames.add(logGroup.logGroupName)
    }

    override fun writeGroupNameIntroduction() {}
}