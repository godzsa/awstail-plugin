package tech.central.ai.writers

import com.amazonaws.services.logs.model.FilteredLogEvent
import com.intellij.openapi.application.ApplicationManager
import com.intellij.util.concurrency.AppExecutorUtil
import tech.central.ai.awstail.AWSLogsClient
import tech.central.ai.awstail.writers.LogWriter
import tech.central.ai.components.LogsComponent
import java.time.Instant
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit

class DialogLogWriter(awsLogs: AWSLogsClient, val logsComponent: LogsComponent) : LogWriter(awsLogs) {

    @Volatile
    private var shouldRun: Boolean = true

    private val scheduler: ScheduledExecutorService = AppExecutorUtil.createBoundedScheduledExecutorService("DialogLogWriter Scheduler", 1)

    override fun tailLogs() {
        if (shouldRun) {
            fetchLogs()
            scheduler.schedule({ tailLogs() }, 1, TimeUnit.SECONDS)
        }
    }

    override fun writeEvent(event: FilteredLogEvent) =
            ApplicationManager.getApplication().invokeLater {
                if (event.message.last() != '\n') event.message += '\n';
                logsComponent.addText(event.message)
            }

    override fun writeFailMessage(failMessage: String) =
            ApplicationManager.getApplication().invokeLater { logsComponent.setHeader(failMessage) }

    override fun writeLogsIntroduction() {
        val verb = if (awsLogs.arguments.follow) "Tailing" else "Fetching"
        var text = "$verb logs for ${awsLogs.arguments.logGroupName}... "

        awsLogs.arguments.startTime?.let { text += """Since ${prettyDate(awsLogs.arguments.startTime)} """ }
        awsLogs.arguments.endTime?.let { text += """Until ${prettyDate(awsLogs.arguments.endTime)} """ }

        ApplicationManager.getApplication().invokeLater { logsComponent.setHeader(text) }
    }

    private fun prettyDate(epoch: Long?): String = if (epoch == null) "" else Instant.ofEpochMilli(epoch).toString()

    fun stop() {
        shouldRun = false;
    }
}