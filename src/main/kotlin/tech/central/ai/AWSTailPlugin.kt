package tech.central.ai

import com.intellij.openapi.application.ApplicationManager
import com.intellij.openapi.options.ShowSettingsUtil
import com.intellij.openapi.project.DumbAware
import com.intellij.openapi.project.Project
import com.intellij.openapi.wm.ToolWindow
import com.intellij.openapi.wm.ToolWindowFactory
import com.intellij.openapi.wm.ToolWindowManager
import com.intellij.ui.content.ContentManagerAdapter
import com.intellij.ui.content.ContentManagerEvent
import tech.central.ai.awstail.AWSLogsClient
import tech.central.ai.components.LogsComponent
import tech.central.ai.components.SelectLogGroupComponent
import tech.central.ai.writers.DialogLogWriter
import tech.central.ai.writers.ListGroupWriter
import javax.swing.JComponent

class AWSTailPlugin : ToolWindowFactory, DumbAware {

    private lateinit var awsLogs: AWSLogsClient
    private lateinit var startComponent: SelectLogGroupComponent
    private lateinit var toolWindow: ToolWindow

    override fun createToolWindowContent(project: Project, toolWindow: ToolWindow) {
        this.toolWindow = toolWindow

        //TODO REFACTOR
        ApplicationManager.getApplication().executeOnPooledThread {
            try {
                awsLogs = AWSTailCredentialProvider(project).awsLogsClient
                showGroups(project)

                ApplicationManager.getApplication().invokeLater {
                    addMainTab()
                }
                tries = 0;
            } catch (e: Exception) {
                if (tries < 2) {
                    tries++;
                    e.printStackTrace()
                    ApplicationManager.getApplication().invokeLater {
                        ShowSettingsUtil.getInstance().showSettingsDialog(project, "AWS Tail")
                    }
                }
            }
        }
    }

    private fun addMainTab() = addNewTab(startComponent.layout, "+")

    private fun addNewTab(component: JComponent, title: String, closeable: Boolean = false) {
        val content = toolWindow.contentManager.factory.createContent(component, title, false)

        content.isCloseable = closeable
        toolWindow.contentManager.addContent(content)
        toolWindow.contentManager.setSelectedContent(content)
    }

    private fun okClicked(project: Project) {
        val logsComponent = LogsComponent(project)

        addNewTab(logsComponent, startComponent.tabTitle, true)
        showLog(logsComponent)
    }

    private fun showLog(logsComponent: LogsComponent) {
        val dialogLogWriter = DialogLogWriter(awsLogs, logsComponent)

        toolWindow.contentManager.addContentManagerListener(object : ContentManagerAdapter() {
            override fun contentRemoved(event: ContentManagerEvent?) {
                dialogLogWriter.stop()
                toolWindow.contentManager.removeContentManagerListener(this)
            }
        })

        dialogLogWriter.write()
    }

    private fun showGroups(project: Project) {
        ListGroupWriter(awsLogs, mutableListOf()).also {
            it.write()
            startComponent = SelectLogGroupComponent(it.logGroupNames, project)
            startComponent.okButton.addActionListener {
                awsLogs.arguments = startComponent.getAWSTailArgs();
                okClicked(project)
            }
        }
    }

    companion object {
        val ID: String = "AWS Tail"
        var tries: Int = 0
        fun restart(project: Project) {
            // TODO: review this
            val toolWindow = ToolWindowManager.getInstance(project).getToolWindow("AWS Tail")
            toolWindow.contentManager.removeAllContents(true);
            AWSTailPlugin().createToolWindowContent(project, toolWindow)
        }
    }
}
