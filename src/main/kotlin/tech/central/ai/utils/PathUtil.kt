package tech.central.ai.utils

class PathUtil {
    companion object {
        fun getPath(source: String): String {
            val path: String = source.trim()
            return if (path.startsWith("~")) path.replace("~", System.getProperty("user.home")) else path
        }
    }
}