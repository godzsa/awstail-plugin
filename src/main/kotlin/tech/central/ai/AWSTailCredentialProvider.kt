package tech.central.ai

import com.amazonaws.auth.AWSStaticCredentialsProvider
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider
import com.amazonaws.auth.STSAssumeRoleSessionCredentialsProvider
import com.amazonaws.auth.profile.ProfileCredentialsProvider
import com.amazonaws.services.securitytoken.AWSSecurityTokenServiceClient
import com.intellij.openapi.project.Project
import tech.central.ai.awstail.AWSLogsClient
import tech.central.ai.awstail.AWSTailArguments
import tech.central.ai.components.SettingComponent
import tech.central.ai.settings.AWSTailPersistentStateComponent
import tech.central.ai.utils.PathUtil

class AWSTailCredentialProvider(project: Project) {
    val awsLogsClient: AWSLogsClient

    init {
        val (credentialType, awsConfigPath, profile, roleArn, accessKey, secretKey, region) =
                AWSTailPersistentStateComponent.getInstance(project).state
        val args = AWSTailArguments()

        awsLogsClient = when (credentialType) {
            SettingComponent.PROFILE -> AWSLogsClient(args, ProfileCredentialsProvider(PathUtil.getPath(awsConfigPath), profile).credentials, region)
            SettingComponent.CUSTOM -> createCustomCredentials(roleArn, accessKey, secretKey, region)
            else -> AWSLogsClient(args, EnvironmentVariableCredentialsProvider().credentials, region)
        }
    }

    private fun createCustomCredentials(roleArn: String, accessKey: String, secretKey: String, region: String): AWSLogsClient {
        val args = AWSTailArguments()

        return if (roleArn.isBlank()) {
            AWSLogsClient(args, BasicAWSCredentials(accessKey, secretKey), region)
        } else {
            val credentials = STSAssumeRoleSessionCredentialsProvider.Builder(roleArn, "assumedRole")
                    .withStsClient(AWSSecurityTokenServiceClient
                            .builder()
                            .withRegion(region)
                            .withCredentials(AWSStaticCredentialsProvider(BasicAWSCredentials(accessKey, secretKey)))
                            .build())
                    .build().credentials
            AWSLogsClient(args, credentials, region)
        }
    }
}